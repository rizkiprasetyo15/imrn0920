console.log("========== No. 1 ==========")
console.log("Looping Pertama")
var i = 2;
while (i<=20){
    console.log(i+ " - I love coding");
    i+=2;
}

console.log("Looping Kedua")
var i = 20;
while (i>=2){
    console.log(i+ " - I will become a mobile developer");
    i-=2;
}

console.log("========== No. 2 ==========")
var santai = " - Santai"
var berkualitas = " - Berkualitas"
var lovecoding = " - I Love Coding"

for (i= 1; i<=20; i++){
    if ( i % 2 !=1){
        console.log(i+ berkualitas)
    } else if (i % 3 == 0){
        console.log(i+ lovecoding)
    } else {
        console.log(i+ santai)
    }
}

console.log("========== No. 3 ==========")
var i = 1;
var j = 1;
var panjang = 8;
var lebar = 4;
var pagar = " ";

while (j <= lebar){
    while (i <= panjang){
        pagar += "#";
        i++;
    }
    console.log(pagar);
    pagar = ' ';
    i=1;
    j++;
}

console.log("========== No. 4 ==========")
var i = 1;
var j = 1;
var alas = 7;
tinggi = 7;
var pagar = "";

for (i=1; i<=tinggi; i++){
    for (j= 1; j<=i; j++){
        pagar +="#";
    }
    console.log(pagar)
    pagar="";
}

console.log("========== No. 5 ==========")
i = 1;
j = 1;
var panjang = 8;
var lebar = 8;
var papan = "";

for (j= 1; j<= lebar; j++){
    if (j%2==1){
        for (i=1; i<=panjang; i++){
            if (i%2==1){
                papan +=" "
            } else {
                papan +="#"
            }
        }
    } else {
        for (i = 1; i<= panjang; i++){
            if (i%2==1){
                papan +="#"
            } else {
                papan +=" "
            }
    }
}
    console.log(papan);
    papan = "";
}