console.log("========== No. 1 ==========")

const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()

  console.log("========== No. 2 ==========")

  const fullName = 'William Imoh'
  const William = {fullName}
   
  //Driver Code 
  console.log(fullName)

  console.log("========== No. 3 ==========")

  let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

    const firstName = newObject.firstName;
    const lastName = newObject.lastName;
    const destination = newObject.destination;
    const occupation = newObject.occupation;
    const spell = newObject.spell;

// Driver code
console.log(firstName, lastName, destination, occupation, spell)

console.log("========== No. 4 ==========")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)

console.log("========== No. 5 ==========")
  
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet  
     consectetur adipiscing elit ${planet} do eiusmod tempor
     incididunt ut labore et dolore magna aliqua Ut enim
     ad minim veniam`
 
// Driver Code
console.log(before)