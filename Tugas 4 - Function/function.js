console.log("=========== No. 1 ===========")
console.log("")
console.log("")

function teriak() {
    return "Halo Sanbers!"
}
console.log(teriak())
console.log(" ")
console.log(" ")

console.log("=========== No. 2 ===========")
console.log(" ")
console.log(" ")

function kalikan (x, y){
    return x * y
}
var num1 = 12
var num2 = 4
var hasilPerkalian = kalikan(num1, num2)
console.log(hasilPerkalian);

console.log(" ")
console.log(" ")

console.log("=========== No. 3 ===========")
console.log(" ")
console.log(" ")

function introduce (name, age, address, hobby){
    var kalimat = "Nama saya" + " "+ name + ","+ " umur saya " + " "+ age + " tahun"+ ","+" "+ "alamat saya "+" "+ address + ","+ "dan saya punya hobby yaitu"+" "+ hobby+ "!"
    return kalimat
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)

console.log(" ")
console.log(" ")
